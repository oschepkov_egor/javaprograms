public class PackagedSetOfGoods implements IGood{
    PackagedOfGood packagedOfGood;
    IGood[] goods;

    public PackagedSetOfGoods(PackagedOfGood packagedOfGood, IGood[] goods) {
        this.packagedOfGood = packagedOfGood;
        this.goods = goods;
    }

    public PackagedOfGood getPackagedOfGood() {
        return packagedOfGood;
    }

    public void setPackagedOfGood(PackagedOfGood packagedOfGood) {
        this.packagedOfGood = packagedOfGood;
    }

    public IGood[] getGoods() {
        return goods;
    }

    public void setGoods(IGood[] goods) {
        this.goods = goods;
    }

    @Override
    public double getBrutto() {
        double brutto = packagedOfGood.getWeight();
        for (IGood good: goods){
            brutto += good.getBrutto();
        }
        return brutto;
    }

    @Override
    public double getNetto() {
        double netto = 0;
        for (IGood good: goods){
            netto += good.getBrutto();
        }
        return netto;
    }

    @Override
    public String getName() {
        return null;
    }
}
