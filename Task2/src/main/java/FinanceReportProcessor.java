public class FinanceReportProcessor {
    public static FinanceReport getFinanceReportByChar(FinanceReport financeReport, char c) {
        int count = 0;
        for (Payment payment : financeReport.getPayments()) {
            if (payment.getFullName().charAt(0) == c) {
                count++;
            }
        }
        Payment[] payments = new Payment[count];
        int i = 0;
        for (Payment payment : financeReport.getPayments()) {
            if (payment.getFullName().charAt(0) == c) {
                payments[i] = payment;
                i++;
            }
        }
        return new FinanceReport(payments, financeReport.getReporterFullName(), financeReport.getReporterDay(),
                                 financeReport.getReporterMonth(), financeReport.getReporterYear()
        );
    }

    public static FinanceReport getFinanceReportBySumma(FinanceReport financeReport, int summa) {
        int count = 0;

        for (Payment payment : financeReport.getPayments()) {
            if (payment.getSumPayment() < summa) {
                count++;
            }
        }
        Payment[] payments = new Payment[count];
        int i = 0;
        for (Payment payment : financeReport.getPayments()) {
            if (payment.getSumPayment() < summa) {
                payments[i] = payment;
                i++;
            }
        }
        return new FinanceReport(payments, financeReport.getReporterFullName(), financeReport.getReporterDay(),
                                 financeReport.getReporterMonth(), financeReport.getReporterYear()
        );
    }

    public static double getFullSummaPaymentByDate(FinanceReport financeReport, String date) {
        double summa = 0;

        String[] formatedDate = date.split("\\.");

        for (Payment payment : financeReport.getPayments()) {
            if (payment.getDay() == Integer.parseInt(formatedDate[0]) && payment.getMonth() == Integer.parseInt(
                    formatedDate[1]) && payment.getYear() == Integer.parseInt(formatedDate[2])) {
                summa += payment.getSumPayment();
            }
        }

        return summa;
    }

    public static String getMonthWithoutPaymentsByYear(FinanceReport financeReport, int year) {
        String[] months =
                {"Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь",
                 "Ноябрь", "Декабрь"};

        for (Payment payment : financeReport.getPayments()) {
            if (payment.getYear() == year && !months[payment.getMonth() - 1].isEmpty()) {
                months[payment.getMonth() - 1] = "";
            }
        }

        StringBuilder sb = new StringBuilder();
        for (String s : months) {
            if (!s.isEmpty()) {
                sb.append(s);
                sb.append(" ");
            }
        }
        sb.deleteCharAt(sb.length() - 1);
        return sb.toString();
    }
}
