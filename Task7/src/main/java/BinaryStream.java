import java.io.*;

public class BinaryStream {
    public static void writeBinary(OutputStream outputStream, int[] arr) throws IOException {
        try (DataOutputStream dataOutputStream = new DataOutputStream(outputStream)) {
            for (int i : arr) {
                dataOutputStream.writeInt(i);
            }
        }
    }

    public static void readBinary(InputStream inputStream, int[] arr) throws IOException {
        try (DataInputStream dataInputStream = new DataInputStream(inputStream)) {
            for (int i = 0; i < arr.length; i++) {
                arr[i] = dataInputStream.readInt();
            }
        }
    }
}
