package ru.omsu;

import java.util.Scanner;

public class Arr {
    public static void main(String[] args) {
        Arr arr = new Arr();
        Scanner s = new Scanner(System.in);
        int a, b, n;
        System.out.println("Введите длину массива ");
        n = s.nextInt();
        System.out.println("Введите элементы массива ");
        arr.fill(n);
        arr.print();
        System.out.println("Сумма всех элементов " + arr.sum());
        System.out.println("Количество чётных " + arr.countEven());
        //System.out.println("Количество положительных " + arr.countPositiv());
        System.out.println("Введите границы ");
        a = s.nextInt();
        b = s.nextInt();
        System.out.println("Число чисел в отрезке " + arr.countRad(a, b));
        System.out.println("Все положительные " + arr.allPositiv());
        arr.reverse();
        arr.print();
    }

    /**/
    private int[] arr;

    public void print() {
        for (int elem : arr) {
            System.out.print(elem + " ");
        }
        System.out.println();
    }

    public void fill(int a) {
        arr = new int[a];
        Scanner s = new Scanner(System.in);
        for (int i = 0; i < arr.length; i++) {
            arr[i] = s.nextInt();
        }
    }

    public int sum() {
        int sum = 0;
        for (int elem : arr) {
            sum += elem;
        }
        return sum;
    }

    public int countEven() {
        int count = 0;
        for (int elem : arr) {
            if (elem % 2 == 0) {
                count++;
            }
        }
        return count;
    }

    public int countPositiv() {
        int count = 0;
        for (int elem : arr) {
            if (elem > 0) {
                count++;
            }
        }
        return count;
    }

    public int countRad(int a, int b) {
        int count = 0;
        for (int elem : arr) {
            if (elem >= a && elem <= b) {
                count++;
            }
        }
        return count;
    }

    public boolean allPositiv() {
        return countPositiv() == arr.length;
    }

    public void reverse() {
        for (int i = 0; i < arr.length / 2; i++) {
            int t = arr[i];
            arr[i] = arr[arr.length - i - 1];
            arr[arr.length - i - 1] = t;

        }
    }
}
