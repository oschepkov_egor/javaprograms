import java.util.*;

public class PhoneBook {
    private Map<String, ArrayList<String>> phoneBook;

    public PhoneBook() {
        phoneBook = new HashMap<>();
    }

    public void addPhone(String fullName, String phone) {
        phoneBook.putIfAbsent(fullName, new ArrayList<>());
        phoneBook.get(fullName).add(phone);
    }

    public void deletePhone(String fullName, String phone) {
        phoneBook.get(fullName).remove(phone);
    }

    public List<String> getListByName(String fullName) {
        return phoneBook.get(fullName);
    }

    public String getNameByPhone(String phone) {
        for (String name : phoneBook.keySet()) {
            if (phoneBook.get(name).contains(phone)) {
                return name;
            }
        }
        return "";
    }

    public PhoneBook getMapByBeginName(String beginName) {
        PhoneBook resultPhoneBook = new PhoneBook();
        for (String name : phoneBook.keySet()) {
            if (name.startsWith(beginName)) {
                for (String phone : phoneBook.get(name)) {
                    resultPhoneBook.addPhone(name, phone);
                }
            }
        }
        return resultPhoneBook;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PhoneBook phoneBook1)) return false;
        return Objects.equals(phoneBook, phoneBook1.phoneBook);
    }

    @Override
    public int hashCode() {
        return Objects.hash(phoneBook);
    }
}
