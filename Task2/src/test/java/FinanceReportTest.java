import org.junit.Assert;
import org.junit.Test;

public class FinanceReportTest {
    @Test
    public void TestConstructorCoping() {
        FinanceReport financeReport1 = new FinanceReport(new Payment[]{new Payment("A", 1, 1, 1, 1)}, "A", 1, 1, 1);
        FinanceReport financeReport2 = new FinanceReport(financeReport1);
        System.out.println(financeReport1);
//        Payment[] payments = {new Payment("B", 2, 2, 2, 2)};
//        financeReport2.setReporterFullName("B");
//        financeReport2.setPayments(payments);
        financeReport1.getPayment(0).setSumPayment(10000);
        Assert.assertNotEquals(financeReport1.getPayment(0).getSumPayment(), financeReport2.getPayment(0).getSumPayment());
        //Assert.assertArrayEquals(financeReport1.getPayments(), financeReport2.getPayments());
    }
}
