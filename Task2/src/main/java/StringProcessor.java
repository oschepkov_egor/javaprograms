import java.io.IOException;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringProcessor {
    public static String copyString(String s, int N) {
        if (N < 0) {
            throw new IllegalArgumentException("Negativ parametr N");
        }
        return String.valueOf(s).repeat(N);
    }

    public static int countEntry(String s1, String s2) {
        if (s2 == null || s2.equals("")) {
            throw new IllegalArgumentException("Wrong s2");
        }
        int count = 0;
        int index = s1.indexOf(s2);
        while (index != -1) {
            count++;
            index = s1.indexOf(s2, index + 1);
        }
        return count;
    }

    public static String replaceString(String s) {
        String str = s;
        str = str.replace("1", "один");
        str = str.replace("2", "два");
        str = str.replace("3", "три");
        return str;
    }

    public static StringBuilder deleteEverySecondChars(StringBuilder sb) {
        for (int i = sb.length() - sb.length() % 2 - 1; i >= 0; i -= 2) {
            sb.deleteCharAt(i);
        }

        return sb;
    }

    public static String reverseWords(String s) {
        int countSpaceBegin = 0;
        int countSpaceEnd = 0;
        StringBuilder reverseString = new StringBuilder("");
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == ' ') {
                countSpaceBegin++;
            } else {
                break;
            }
        }

        for (int i = s.length() - 1; i >= 0; i--) {
            if (s.charAt(i) == ' ') {
                countSpaceEnd++;
            } else {
                break;
            }
        }

        s = s.trim();
        String[] words = s.split(" ");
        int k = -1;
        for (int i = words.length - 1; i >= 0; i--) {
            if (i <= k) {
                break;
            }
            if (words[i].equals("")) {
                continue;
            }
            for (int j = k + 1; j < words.length; j++) {
                if (words[j].equals("")) {
                    continue;
                }
                String t = words[j];
                words[j] = words[i];
                words[i] = t;
                k = j;
                break;
            }
        }

        reverseString.append(copyString(" ", countSpaceBegin));
        for (int i = 0; i < words.length - 1; i++) {
            reverseString.append(words[i]);
            reverseString.append(" ");
        }
        if (words.length != 0) {
            reverseString.append(words[words.length - 1]);
        }
        reverseString.append(copyString(" ", countSpaceEnd));

        return reverseString.toString();
    }

    public static String convertHexString(String s) {
        StringBuilder sb = new StringBuilder(s);
        String chars = "0123456789ABCDEF";
        String regex = "0x[0-9A-F]+";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(sb.toString());
        System.out.println("_________________________________");
        System.out.println(sb.toString());
        while (matcher.find()) {
            int indexBegin = sb.indexOf("0x");
            int indexEnd = indexBegin + 2;

            for (int i = indexBegin + 2; i < sb.length(); i++) {
                if (chars.indexOf(sb.charAt(i)) == -1) {
                    break;
                }
                indexEnd++;
            }
            String numberStr = sb.substring(indexBegin + 2, indexEnd);
            System.out.println(numberStr);
            int number = 0;
            for (int i = 0; i < numberStr.length(); i++) {
                number += chars.indexOf(numberStr.charAt(i)) * Math.pow(16, numberStr.length() - i - 1);
            }
            sb.replace(indexBegin, indexEnd, String.valueOf(number));
            matcher = pattern.matcher(sb.toString());
        }
        System.out.println(sb);
        return sb.toString();
    }
}
