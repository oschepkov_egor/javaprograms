import org.junit.Assert;
import org.junit.Test;

public class FunctionTest {
    @Test
    public void testFunctionLinean() {
        LineanFunction lineanFunction = new LineanFunction(1, 3, -5, 5);
        Assert.assertEquals(4, lineanFunction.getValue(1), 1e-4);
    }

    @Test
    public void testFunctionSinus() {
        SinusFunction sinusFunction = new SinusFunction(1, 1, -5, 5);
        Assert.assertEquals(1, sinusFunction.getValue(Math.PI/2), 1e-4);
    }

    @Test
    public void testFunctionRational() {
        RationalFunction rationalFunction = new RationalFunction(1, 5, 1, 1, -5, 5);
        Assert.assertEquals(3, rationalFunction.getValue(1), 1e-4);
    }

    @Test
    public void testFunctionExp() {
        ExpFunction expFunction = new ExpFunction(1, 0, -5, 5);
        Assert.assertEquals(2.7182, expFunction.getValue(1),1e-4);
    }
}
