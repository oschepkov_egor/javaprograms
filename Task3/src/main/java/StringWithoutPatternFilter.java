public class StringWithoutPatternFilter implements IFilter{

    private String pattern;

    public StringWithoutPatternFilter(String pattern) {
        this.pattern = pattern;
    }

    @Override
    public boolean apply(String s) {
        return !s.contains(pattern);
    }
}
