public class SummaValuesFunction implements IFunctionalWithOneArgument{

    @Override
    public double getFunctional(IFunctionWithOneArgument func) {
        double begin = func.getBegin();
        double end = func.getEnd();
        return func.getValue(begin) + func.getValue(end) + func.getValue((begin + end)/2);
    }

}
