import org.junit.Assert;
import org.junit.Test;

import java.io.*;

public class BinaryStreamTest {
    @Test
    public void readWriteBinaryTest() throws IOException {
        int[] a = {1, 2, 300};
        try (OutputStream outputStream = new FileOutputStream("a.bin")) {
            BinaryStream.writeBinary(outputStream, a);
        }
        int[] ints = new int[a.length];
        try (InputStream inputStream = new FileInputStream("a.bin")){
            BinaryStream.readBinary(inputStream, ints);
        }
        Assert.assertArrayEquals(a, ints);
    }

}