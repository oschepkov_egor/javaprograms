import org.junit.Assert;
import org.junit.Test;

public class FilterTest {
    @Test
    public void beginStringFilterTest(){
        BeginStringFilter beginStringFilter = new BeginStringFilter("Hell");
        Assert.assertTrue(beginStringFilter.apply("Hello world"));
        Assert.assertFalse(beginStringFilter.apply("Hi world"));
        Assert.assertFalse(beginStringFilter.apply("World hello"));
    }

    @Test
    public void endStringFilterTest() {
        EndStringFilter endStringFilter = new EndStringFilter("ello");
        Assert.assertTrue(endStringFilter.apply("World hello"));
        Assert.assertFalse(endStringFilter.apply("Hello world"));
        Assert.assertFalse(endStringFilter.apply("Hi world"));
    }

    @Test
    public void stringWithoutPatternFilterTest() {
        StringWithoutPatternFilter stringWithoutPatternFilter = new StringWithoutPatternFilter("Bye");
        Assert.assertTrue(stringWithoutPatternFilter.apply("Hello world"));
        Assert.assertFalse(stringWithoutPatternFilter.apply("Bye world"));
        Assert.assertTrue(stringWithoutPatternFilter.apply("Goodbye world"));
    }
}
