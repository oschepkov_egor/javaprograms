import org.junit.Assert;
import org.junit.Test;

import java.util.*;

public class StreamApiTest {
    @Test
    public void removeNullTest() {
        List<Object> objectList = new ArrayList<>();
        objectList.add(null);
        objectList.add(new Human("a", "a", "a", 1, Sex.MALE));
        Assert.assertEquals(List.of(new Human("a", "a", "a", 1, Sex.MALE)),
                            LambdaRunner.biFunction(StreamApiDemo.removeNull, objectList)
                           );
    }

    @Test
    public void countPositiveTest() {
        Assert.assertEquals(Optional.of(2L),
                            Optional.of(LambdaRunner.biFunction(StreamApiDemo.countPositive, Set.of(1, 2, -1)))
                           );
    }

    @Test
    public void getThreeLastTest() {
        Assert.assertArrayEquals(new Object[]{2, 3, 4},
                                 LambdaRunner.biFunction(StreamApiDemo.getThreeLast, List.of(1, 2, 3, 4))
                                );
    }

    @Test
    public void getThreeLastTest1() {
        Assert.assertArrayEquals(new Object[]{3, 4},
                                 LambdaRunner.biFunction(StreamApiDemo.getThreeLast, List.of(3, 4))
                                );
    }

    @Test
    public void getFirstPositiveTest() {
        Assert.assertEquals(Optional.of(2),
                            Optional.of(LambdaRunner.biFunction(StreamApiDemo.getFirstEven, List.of(-1, 2)))
                           );
        Assert.assertEquals(Optional.empty(), Optional.ofNullable(
                LambdaRunner.biFunction(StreamApiDemo.getFirstEven, List.of(-1, 3))));
    }

    @Test
    public void getListOfSquaresTest() {
        Assert.assertEquals(List.of(1, 4, 9),
                            LambdaRunner.biFunction(StreamApiDemo.getListOfSquares, new Integer[]{1, 2, 2, 3})
                           );
    }

    @Test
    public void getListOfSortedStrings() {
        List<String> stringList = new ArrayList<>(List.of("c", "b", "a", ""));
        stringList.add(null);
        Assert.assertEquals(List.of("a", "b", "c"),
                            LambdaRunner.biFunction(StreamApiDemo.getListOfSortedStrings, stringList)
                           );
    }

    @Test
    public void getListOfStringReverseSortTest() {
        Assert.assertEquals(List.of("c", "b", "a"),
                            LambdaRunner.biFunction(StreamApiDemo.getListOfStringReverseSort, Set.of("a", "c", "b"))
                           );
    }

    @Test
    public void getSumOfSquaresTest() {
        Assert.assertEquals(Optional.of(30),
                            Optional.of(LambdaRunner.biFunction(StreamApiDemo.getSumOfSquares, Set.of(1, 2, 3, 4)))
                           );
    }

    @Test
    public void getMaxAge() {
        Collection<Human> humans = new ArrayList<>(
                List.of(new Human("a", "a", "a", 10, Sex.MALE), new Human("a", "a", "a", 30, Sex.FEMALE)));
        Assert.assertEquals(Optional.of(30), Optional.of(LambdaRunner.biFunction(StreamApiDemo.getMaxAge, humans)));
    }

    @Test
    public void getSortedHumans() {
        Collection<Human> humans = new ArrayList<>(
                List.of(new Human("a", "a", "a", 10, Sex.MALE), new Human("a", "a", "a", 12, Sex.FEMALE),
                        new Human("a", "a", "a", 15, Sex.MALE)
                       ));
        Assert.assertEquals(new ArrayList<>(
                List.of(new Human("a", "a", "a", 10, Sex.MALE), new Human("a", "a", "a", 15, Sex.MALE),
                        new Human("a", "a", "a", 12, Sex.FEMALE)
                       )), LambdaRunner.biFunction(StreamApiDemo.getSortedHumans, humans));
    }
}
