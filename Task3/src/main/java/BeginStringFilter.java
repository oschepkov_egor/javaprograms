import java.util.Objects;

public class BeginStringFilter implements IFilter{

    private String pattern;

    public BeginStringFilter(String pattern) {
        this.pattern = pattern;
    }

    public String getPattern() {
        return pattern;
    }

    public void setPattern(String pattern) {
        this.pattern = pattern;
    }

    @Override
    public boolean apply(String s) {
        return s.startsWith(pattern);
    }
}
