import org.junit.Assert;
import org.junit.Test;
import ru.omsu.*;

import java.util.Arrays;

public class MatrixTest {

    @Test
    public void getDet() {
        Matrix matrix = new Matrix(3);
        matrix.setElem(0, 0, 0);
        matrix.setElem(2, 0, 1);
        matrix.setElem(3, 0, 2);
        matrix.setElem(4, 1, 0);
        matrix.setElem(5, 1, 1);
        matrix.setElem(6, 1, 2);
        matrix.setElem(7, 2, 0);
        matrix.setElem(8, 2, 1);
        matrix.setElem(9, 2, 2);
        Assert.assertEquals(3, matrix.getDet(), 1e-6);
    }

    @Test
    public void getDet2() {
        Matrix matrix = new Matrix(3);
        matrix.setElem(1, 0, 0);
        matrix.setElem(0, 0, 1);
        matrix.setElem(0, 0, 2);
        matrix.setElem(0, 1, 0);
        matrix.setElem(0, 1, 1);
        matrix.setElem(1, 1, 2);
        matrix.setElem(0, 2, 0);
        matrix.setElem(0, 2, 1);
        matrix.setElem(2, 2, 2);
        Assert.assertEquals(0, matrix.getDet(), 1e-6);
    }

    @Test
    public void getDet1() {
        Matrix matrix = new Matrix(2);
        matrix.setElem(0, 0, 0);
        matrix.setElem(2, 0, 1);
        matrix.setElem(-3, 1, 0);
        matrix.setElem(4, 1, 1);
        Assert.assertEquals(6, matrix.getDet(), 1e-6);
    }

    @Test
    public void getDet3() {
        UpTriangleMatrix upTriangleMatrix = new UpTriangleMatrix(2);
        upTriangleMatrix.setElem(1, 0, 0);
        upTriangleMatrix.setElem(2, 0, 1);
        upTriangleMatrix.setElem(-4, 1, 1);
        IMatrix[] matrices = new IMatrix[]{new DiagMatrix(2, new double[]{1, 3}), upTriangleMatrix,
                                           new DiagMatrix(2, new double[]{1, 2})};
        MatrixService.sortArrayMatrix(matrices);
        Assert.assertArrayEquals(new IMatrix[]{upTriangleMatrix, new DiagMatrix(2, new double[]{1, 2}),
                                               new DiagMatrix(2, new double[]{1, 3})}, matrices);
    }
}