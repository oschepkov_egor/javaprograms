public class SquareTrinomialWithBiggerRoots {
    SquareTrinomial squareTrinomial;

    public double getBiggerRoot(){
        double[] roots = squareTrinomial.getRoots();
        double res;
        if(roots.length == 2){
            res = Math.max(roots[0], roots[1]);
        }else if(roots.length == 1){
            res = roots[0];
        }else{
            throw new IllegalArgumentException("no roots");
        }
        return res;
    }

    public SquareTrinomialWithBiggerRoots(SquareTrinomial squareTrinomial) {
        this.squareTrinomial = squareTrinomial;
    }

    public SquareTrinomial getSquareTrinomial() {
        return squareTrinomial;
    }

    public void setSquareTrinomial(SquareTrinomial squareTrinomial) {
        this.squareTrinomial = squareTrinomial;
    }

}
