import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class CharStream {
    public static void writeStream(Writer writer, int[] arr) throws IOException {
        for (int i : arr) {
            writer.write(Integer.toString(i));
            writer.write(' ');
        }
    }

    public static void readStream(Reader reader, int[] arr) throws IOException {
        try(BufferedReader stream = new BufferedReader(reader)){
            String s = stream.readLine();
            String[] numbers = s.split(" ");
            for(int i = 0; i < arr.length; i++){
                arr[i] = Integer.parseInt(numbers[i]);
            }
        }
    }
}
