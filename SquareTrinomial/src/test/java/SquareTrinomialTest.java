import org.junit.Assert;
import org.junit.Test;

public class SquareTrinomialTest {
    @Test
    public void testGetRoots() {
        SquareTrinomial squareTrinomial = new SquareTrinomial(1, -2, 1);
        Assert.assertArrayEquals(new double[]{1}, squareTrinomial.getRoots(), 1e-4);
    }

    @Test
    public void testGetRoot() {
        SquareTrinomial squareTrinomial = new SquareTrinomial(1, 2, -3);
        Assert.assertArrayEquals(new double[]{1, -3}, squareTrinomial.getRoots(), 1e-4);
    }
}