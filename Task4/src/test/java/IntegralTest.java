import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class IntegralTest {

    @Test
    public void getFunctionaLinean() {
        LineanFunction lineanFunction = new LineanFunction(2, -2, 1, 3);
        Integral integral = new Integral(1, 2);
        Assert.assertEquals(1, integral.getFunctional(lineanFunction), 1e-2);
    }

    @Test
    public void getFunctionaSinus() {
        SinusFunction sinusFunction = new SinusFunction(2, 2, 0, 5);
        Integral integral = new Integral(0, 5);
        Assert.assertEquals(1.86450, integral.getFunctional(sinusFunction), 1e-4);
    }

    @Test
    public void getFunctionaRational() {
        RationalFunction ratioanalFunction = new RationalFunction(2, 3, 1, 2, -3, 0);
        Integral integral = new Integral(-3, 0);
        Assert.assertEquals(3.515275, integral.getFunctional(ratioanalFunction), 1e-4);
    }

    @Test
    public void getFunctionaExp() {
        ExpFunction expFunction = new ExpFunction(1, 2, 1, 3);
        Integral integral = new Integral(1, 3);
        Assert.assertEquals(21.19586, integral.getFunctional(expFunction), 1e-4);
    }
}