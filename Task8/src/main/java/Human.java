import java.util.Objects;

public class Human implements Comparable<Human>{
    private String surname;
    private String name;
    private String middlename;
    private int age;

    public Human(String surname, String name, String lastname, int age) {
        this.surname = surname;
        this.name = name;
        this.middlename = lastname;
        this.age = age;
    }

    public Human(Human human) {
        surname = human.getSurname();
        name = human.getName();
        middlename = human.getMiddlename();
        age = human.getAge();
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Human human)) return false;
        return age == human.age && Objects.equals(surname, human.surname) && Objects.equals(name, human.name) && Objects.equals(middlename, human.middlename);
    }

    @Override
    public int hashCode() {
        return Objects.hash(surname, name, middlename, age);
    }


    @Override
    public int compareTo(Human o) {
        int comp = surname.compareTo(o.getSurname());
        if(comp == 0){
            comp = name.compareTo(o.getName());
            if(comp == 0){
                comp = middlename.compareTo(o.getMiddlename());
                if(comp == 0){
                    comp = age - o.getAge();
                }
            }
        }
        return comp;
    }
}
