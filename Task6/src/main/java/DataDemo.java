import java.util.ArrayList;
import java.util.Iterator;

public class DataDemo {
    public static ArrayList<Integer> getAll(Data data) {

        var res = new ArrayList<Integer>();
        for (Integer i : data) {
            res.add(i);
        }
        return res;
    }
}
