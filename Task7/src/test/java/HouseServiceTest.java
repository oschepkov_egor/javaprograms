import org.junit.Assert;
import org.junit.Test;

import java.io.*;
import java.util.List;

public class HouseServiceTest {

    @Test
    public void serialHouse() throws IOException, ClassNotFoundException {
        House house = new House("123", 1, new Person("A", "a", "c", "1.1.1"),
                                List.of(new Flat(1,2,
                                                 List.of(new Person("a","a", "a", "1.1.1")))));
        try (ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream("house.txt"))) {
            HouseService.serialHouse(stream, house);
        }
        House resultHouse;
        try (ObjectInputStream stream = new ObjectInputStream(new FileInputStream("house.txt"))){
            resultHouse = HouseService.deserialHouse(stream);
        }
        Assert.assertEquals(house, resultHouse);
    }

    @Test
    public void serialHouseJSON() throws IOException {
        House house = new House("123", 1, new Person("A", "a", "c", "1.1.1"),
                                List.of(new Flat(1,2,
                                                 List.of(new Person("a","a", "a", "1.1.1")))));
        System.out.println(HouseService.serialHouseJSON(house));
        Assert.assertEquals(house, HouseService.deserialHouseJSON(HouseService.serialHouseJSON(house)));
    }
}