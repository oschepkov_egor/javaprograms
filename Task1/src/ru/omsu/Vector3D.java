package ru.omsu;

import java.util.Objects;

public class Vector3D {
    private double x;
    private double y;
    private double z;

    public double length(){
        return Math.sqrt(x*x + y*y + z*z);
    }

    public Vector3D(double xStart, double yStart, double zStart, double xEnd, double yEnd, double zEnd) {
        this.x = xEnd - xStart;
        this.y = yEnd - yStart;
        this.z = zEnd - zStart;
    }

    public Vector3D(Point3D start, Point3D end) {
        setX(end.getX() - start.getX());
        setY(end.getY() - start.getY());
        setZ(end.getZ() - start.getZ());
    }

    public Vector3D() {
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    @Override
    public String toString() {
        return "Vector3D{" +
                "x=" + x +
                ", y=" + y +
                ", z=" + z +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vector3D vector3D = (Vector3D) o;
        double eps = 1e-4;
        return Math.abs(vector3D.x - x) < eps && Math.abs(vector3D.y - y) < eps && Math.abs(vector3D.z - z) < eps;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, z);
    }
}
