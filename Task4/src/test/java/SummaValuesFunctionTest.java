import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class SummaValuesFunctionTest {

    @Test
    public void getSummaTest() {
        LineanFunction lineanFunction = new LineanFunction(2, -2, -5, 5);
        SummaValuesFunction summaValuesFunction = new SummaValuesFunction();
        Assert.assertEquals(-6, summaValuesFunction.getFunctional(lineanFunction), 1e-4);
    }
}