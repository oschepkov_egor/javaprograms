package ru.omsu;

import java.util.Arrays;
import java.util.Collections;
import java.util.Objects;

public class Matrix implements IMatrix {

    private int n;
    private double[] matrix;
    private double det;
    private boolean flagDet = false;

    public Matrix(int n) {
        this.n = n;
        matrix = new double[n * n];
    }

    public double[] getMatrix() {
        return matrix;
    }

    @Override
    public int getSize() {
        return n;
    }

    @Override
    public double getElem(int i, int j) {
        if (i < 0 && j < 0 && i >= n && j >= n) {
            throw new IllegalArgumentException("invalid index");
        }
        return matrix[i * n + j];
    }

    @Override
    public void setElem(double v, int i, int j) {

        if (i < 0 && j < 0 && i >= n && j >= n) {
            throw new IllegalArgumentException("invalid index");
        }
        matrix[i * n + j] = v;
        flagDet = false;
    }

    private void sortStrings(int string, int column) {
        for (int k = n; k > 0; k--) {
            for (int i = n - k + string; i < n - 1; i++) {
                if (Math.abs(matrix[i * n + column]) < Math.abs(matrix[(i + 1) * n + column])) {
                    for (int j = 0; j < n; j++) {
                        double t = matrix[i * n + j];
                        matrix[i * n + j] = matrix[(i + 1) * n + j];
                        matrix[(i + 1) * n + j] = t;
                    }
                    det *= (-1);
                }
            }
        }
    }

    @Override
    public double getDet() {
        if (flagDet) {
            return det;
        }
        det = 1;
        for (int j = 0; j < n - 1; j++) {
            sortStrings(j, j);
            for (int i = 1 + j; i < n; i++) {
                double t = -1 * matrix[i * n + j] / matrix[j * n + j];
                if (matrix[i * n + j] != 0) {
                    for (int k = 0; k < n; k++) {
                        matrix[i * n + k] += t * matrix[j * n + k];
                    }
                }
            }
        }
        flagDet = true;
        for (int i = 0; i < n; i++) {
            det *= matrix[i * n + i];
        }
        return det;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        for (int i = 0; i < n; i++) {
            s.append("{");
            for (int j = 0; j < n; j++) {
                s.append(matrix[i * n + j]);
                s.append(", ");
            }
            s.delete(s.length() - 2, s.length() - 1);
            s.append("}\n");
        }
        return "Matrix{" +
               "matrix=\n" + s +
               '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Matrix matrix1)) return false;
        return n == matrix1.n &&
               Arrays.equals(matrix, matrix1.matrix);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(n);
        result = 31 * result + Arrays.hashCode(matrix);
        return result;
    }
}
