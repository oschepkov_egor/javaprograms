public class SinusFunction implements IFunctionWithOneArgument {
    private double a;
    private double b;
    private double begin;
    private double end;

    @Override
    public double getValue(double x) {
        if(x < begin || x > end) throw new IllegalArgumentException("x isn't [begin, end]");
        return a*Math.sin(b*x);
    }

    @Override
    public double getBegin() {
        return begin;
    }

    @Override
    public double getEnd() {
        return end;
    }

    public SinusFunction(double a, double b, double begin, double end) {
        this.a = a;
        this.b = b;
        this.begin = begin;
        this.end = end;
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public void setBegin(double begin) {
        this.begin = begin;
    }

    public void setEnd(double end) {
        this.end = end;
    }
}
