import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FileStream {
    public static List<String> getListOfFilesByType(String path, String type){
        File dir = new File(path);
        String[] files = dir.list();
        List<String> list = new ArrayList<>();
        if(files != null) {
            for (String file : files) {
                if (file.endsWith(type)) {
                    list.add(file);
                }
            }
        }
        return list;
    }


    public static List<String> getListOfFileByReg(String path, String regex){
        List<String> resultList = new ArrayList<>();
        File dir = new File(path);
        File[] files = dir.listFiles();
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher;
        if(files != null) {
            for (File file : files) {
                matcher = pattern.matcher(file.getName());
                if(matcher.matches()){
                    resultList.add(file.getAbsolutePath());
                }
                if(file.isDirectory()){
                    resultList.addAll(getListOfFileByReg(file.getAbsolutePath(), regex));
                }
            }
        }
        return resultList;
    }
}
