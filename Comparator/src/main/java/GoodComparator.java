import java.util.Comparator;

public class GoodComparator implements Comparator<Good> {
    public int compare(Good good1, Good good2) {
        int nameCompare = good1.getName().compareTo(good2.getName());
        if (nameCompare == 0) {
            return good1.getDescription().compareTo(good2.getDescription());
        }
        return nameCompare;
    }
}
