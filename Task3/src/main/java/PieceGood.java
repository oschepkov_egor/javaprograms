import java.util.Objects;

public class PieceGood extends Good{
    private double weightByOne;

    public PieceGood(String name, String description, double weightByOne) {
        super(name, description);
        this.weightByOne = weightByOne;
    }

    public double getWeightByOne() {
        return weightByOne;
    }

    public void setWeightByOne(double weightByOne) {
        this.weightByOne = weightByOne;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PieceGood)) return false;
        if (!super.equals(o)) return false;
        PieceGood pieceGood = (PieceGood) o;
        return Double.compare(pieceGood.weightByOne, weightByOne) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), weightByOne);
    }
}
