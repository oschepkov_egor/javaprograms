import java.nio.charset.StandardCharsets;
import java.util.Comparator;

public class HumanComparator implements Comparator<Human> {

    @Override
    public int compare(Human human1, Human human2) {
        int nameCompare = human1.getSurname().compareTo(human2.getSurname());
        if (nameCompare == 0) {
            nameCompare = human1.getName().compareTo(human2.getName());
            if(nameCompare == 0) {
                return human1.getMiddlename().compareTo(human2.getMiddlename());
            }
        }
        return nameCompare;
    }
}
