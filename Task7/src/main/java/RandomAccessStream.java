import java.io.IOException;
import java.io.RandomAccessFile;

public class RandomAccessStream {
    public static void readStream(RandomAccessFile stream, int[] arr, int pos) throws IOException {
        stream.seek(pos * 4L);
        for (int i = 0; i < arr.length - pos + 1; i++) {
            int b = stream.readInt();
            arr[i] = b;
        }
    }
}
