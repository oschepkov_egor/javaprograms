public class BatchOfGoods {
    private String description;
    private IGood[] packedGoods;

    public double getAllBrutto(){
        double brutto = 0;
        for (IGood good: packedGoods) {
            brutto += good.getBrutto();
        }
        return brutto;
    }

    public BatchOfGoods(String description, IGood[] packedGoods) {
        this.description = description;
        this.packedGoods = packedGoods;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public IGood[] getPackedGoods() {
        return packedGoods;
    }

    public void setPackedGoods(IGood[] packedGoods) {
        this.packedGoods = packedGoods;
    }
}