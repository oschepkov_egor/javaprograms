import java.io.Serializable;
import java.util.Objects;

public class Person implements Serializable {
    private String surname;
    private String name;
    private String middlename;
    private String birthday;

    public Person(){}

    public Person(String surname, String name, String middlename, String birthday) {
        this.surname = surname;
        this.name = name;
        this.middlename = middlename;
        this.birthday = birthday;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Person person)) return false;
        return Objects.equals(surname, person.surname) && Objects.equals(name, person.name) && Objects.equals(middlename, person.middlename) && Objects.equals(birthday, person.birthday);
    }

    @Override
    public int hashCode() {
        return Objects.hash(surname, name, middlename, birthday);
    }
}
