public class ServiceOfGoods {
    public static int countByFilter(BatchOfGoods batchOfGoods, IFilter filter) {
        int count = 0;

        for (IGood good : batchOfGoods.getPackedGoods()) {
            if (filter.apply(good.getName())) {
                count++;
            }
        }
        return count;
    }

    public static boolean checkAllWeighted(BatchOfGoods batchOfGoods) {
        boolean check = true;
        for (IGood good : batchOfGoods.getPackedGoods()) {
            if (!(good instanceof PackagedWeightGood)) {
                check = false;
                break;
            }
        }
        return check;
    }

    public static int countByFilterDeep(BatchOfGoods batchOfGoods, IFilter filter) {
        int count = 0;
        for (IGood good : batchOfGoods.getPackedGoods()) {
            if(PackagedSetOfGoods.class == good.getClass()){
                PackagedSetOfGoods packagedSetOfGoods = (PackagedSetOfGoods) good;
                if(countByFilterDeep(new BatchOfGoods("", packagedSetOfGoods.getGoods()), filter) != 0){
                    count++;
                }
            }else{
                if (filter.apply(good.getName())) {
                    count++;
                }
            }
        }
        return count;
    }
}
