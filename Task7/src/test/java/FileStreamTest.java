import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

public class FileStreamTest {

    @Test
    public void testGetListOfFilesByTypeTest() {
        System.out.println(Arrays.toString(",,,c,,a,,".split(",")));
        List<String> testList = List.of("a.txt", "b.txt");
        Assert.assertEquals(testList, FileStream.getListOfFilesByType("files", "txt"));
    }

    @Test
    public void getListOfFileByRegTest() {
        List<String> testList = List.of("D:\\Projects\\Java\\javaprograms\\Task7\\files\\a.txt",
                                        "D:\\Projects\\Java\\javaprograms\\Task7\\files\\b.txt",
                                        "D:\\Projects\\Java\\javaprograms\\Task7\\files\\test1\\c.txt"
                                       );
        Assert.assertEquals(testList, FileStream.getListOfFileByReg("files", "[abc].txt"));
    }

    @Test
    public void getListOfFileByRegTest1() {
        List<String> testList = List.of("D:\\Projects\\Java\\javaprograms\\Task7\\files\\test1");
        Assert.assertEquals(testList, FileStream.getListOfFileByReg("files", "test1"));
    }
}