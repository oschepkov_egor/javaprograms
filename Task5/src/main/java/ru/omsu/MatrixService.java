package ru.omsu;

import java.util.Arrays;
import java.util.Comparator;

public class MatrixService {
    public static void sortArrayMatrix(IMatrix[] matrices){
        Arrays.sort(matrices, new ComparatorMatrix());
    }
}
