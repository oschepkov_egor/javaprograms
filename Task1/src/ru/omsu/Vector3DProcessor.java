package ru.omsu;

public class Vector3DProcessor {
    static public Vector3D summa(Vector3D vector1, Vector3D vector2) {
        Vector3D vector3 = new Vector3D();
        vector3.setX(vector1.getX() + vector2.getX());
        vector3.setY(vector1.getY() + vector2.getY());
        vector3.setZ(vector1.getZ() + vector2.getZ());
        return vector3;
    }

    static public Vector3D difference(Vector3D vector1, Vector3D vector2) {
        Vector3D vector3 = new Vector3D();
        vector3.setX(vector1.getX() - vector2.getX());
        vector3.setY(vector1.getY() - vector2.getY());
        vector3.setZ(vector1.getZ() - vector2.getZ());
        return vector3;
    }

    static public double scalar(Vector3D vector1, Vector3D vector2) {
        return vector1.getX() * vector2.getX() + vector1.getY() * vector2.getY() + vector1.getZ() * vector2.getZ();
    }

    static public Vector3D vector(Vector3D vector1, Vector3D vector2) {
        Vector3D vector3 = new Vector3D();
        vector3.setX(vector1.getY() * vector2.getZ() - vector1.getZ() * vector2.getY());
        //System.out.println((vector1.getY2() - vector1.getY1()) + " * " + (vector2.getZ2() - vector2.getZ1()) + " - " + (vector1.getZ2() - vector1.getZ1()) + " * " + (vector2.getY2() - vector2.getY1()));
        vector3.setY(vector1.getZ() * vector2.getX() - vector1.getX() * vector2.getZ());
        vector3.setZ(vector1.getX() * vector2.getY() - vector1.getY() * vector2.getX());
        return vector3;
    }

    static public boolean collinear(Vector3D vector1, Vector3D vector2) {
        return vector(vector1, vector2).length() == 0;//(vector1.getX2() - vector1.getX1()) / (vector2.getX2() - vector2.getX1()) == (vector1.getY2() - vector1.getY1()) / (vector2.getY2() - vector2.getY1()) && (vector1.getX2() - vector1.getX1()) / (vector2.getX2() - vector2.getX1()) == (vector1.getZ2() - vector1.getZ1()) / (vector2.getZ2() - vector2.getZ1());
    }
}
