import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class StringProcessorTest {
    @Test
    public void testCopyString() {
        String string1 = StringProcessor.copyString("bed", 3);
        String string2 = StringProcessor.copyString("bed", 2);
        String string3 = StringProcessor.copyString("bed", 0);
        assertEquals("bedbedbed", string1);
        assertEquals("bedbed", string2);
        assertEquals("", string3);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testExceptionCopyString() {
        String string1 = StringProcessor.copyString("bed", -1);
    }


    @Test
    public void testCountEntry() {
        int n1 = StringProcessor.countEntry("abcdabcaba", "a");
        int n2 = StringProcessor.countEntry("abcdabcaba", "b");
        int n3 = StringProcessor.countEntry("abcdabcaba", "e");

        assertEquals(4, n1);
        assertEquals(3, n2);
        assertEquals(0, n3);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testExceptionCountEntry() {
        int n1 = StringProcessor.countEntry("abcdabcaba", "");
        int n2 = StringProcessor.countEntry("abcdabcaba", null);
    }

    @Test
    public void testReplaceString() {
        String string1 = StringProcessor.replaceString("123");
        String string2 = StringProcessor.replaceString("1ab");

        assertEquals("одиндватри", string1);
        assertEquals("одинab", string2);
    }

    @Test
    public void testDeleteEverySecondChars() {
        StringBuilder stringBuilder1 = new StringBuilder("1234567890");
        StringProcessor.deleteEverySecondChars(stringBuilder1);
        StringBuilder stringBuilder2 = new StringBuilder("abcdef");
        StringProcessor.deleteEverySecondChars(stringBuilder2);

        assertEquals("13579", stringBuilder1.toString());
        assertEquals("ace", stringBuilder2.toString());

        assertEquals("", StringProcessor.deleteEverySecondChars(new StringBuilder("")).toString());
        assertEquals("1", StringProcessor.deleteEverySecondChars(new StringBuilder("1")).toString());
        assertEquals("1", StringProcessor.deleteEverySecondChars(new StringBuilder("12")).toString());
        assertEquals("13", StringProcessor.deleteEverySecondChars(new StringBuilder("123")).toString());
    }

    @Test
    public void testReverseWords() {
        String string1 = StringProcessor.reverseWords("  aaa  bbb cc dd");
        String string2 = StringProcessor.reverseWords(" abbb    cc  das ");
        assertEquals("  dd  cc bbb aaa", string1);
        assertEquals(" das    cc  abbb ", string2);

        assertEquals("", StringProcessor.reverseWords(""));
        assertEquals(" a ", StringProcessor.reverseWords(" a "));
        assertEquals("  a", StringProcessor.reverseWords("  a"));
        assertEquals("a  ", StringProcessor.reverseWords("a  "));
        assertEquals(" aa  aa  ", StringProcessor.reverseWords(" aa  aa  "));
        assertEquals("aaa aaa aaaa   ", StringProcessor.reverseWords("aaaa aaa aaa   "));
    }

    @Test
    public void testConvertHexString() {
        /*String string1 = StringProcessor.convertHexString("Васе 0x00000010 лет");
        String string2 = StringProcessor.convertHexString("Петру исполнилось 0x00000011");
        assertEquals("Васе 16 лет", string1);
        assertEquals("Петру исполнилось 17", string2);*/

        assertEquals("16x1", StringProcessor.convertHexString("0x10x1"));
        assertEquals("1", StringProcessor.convertHexString("0x1"));
        assertEquals("16x117", StringProcessor.convertHexString("0x10x10x011"));

        assertEquals(" ", StringProcessor.convertHexString(" "));
        assertEquals(" x", StringProcessor.convertHexString(" x"));
        assertEquals(" 0x", StringProcessor.convertHexString(" 0x"));
        assertEquals(" 0", StringProcessor.convertHexString(" 0x00"));
        assertEquals(" aaa 10 255 bbb", StringProcessor.convertHexString(" aaa 0xA 0xFF bbb"));
    }
}

