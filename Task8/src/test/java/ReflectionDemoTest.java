import org.junit.Assert;
import org.junit.Test;

import javax.management.ObjectName;
import java.lang.reflect.InvocationTargetException;
import java.sql.Ref;
import java.util.*;

import static org.junit.Assert.*;

public class ReflectionDemoTest {

    @Test
    public void countObjects() {
        List<Object> objects = new ArrayList<>();
        objects.add(new Human("a", "a", "a", 1));
        objects.add(new Human("a", "a", "a", 1));
        objects.add(new Human("a", "a", "a", 1));
        objects.add(new Student("a", "a", "a", 1, "a"));
        objects.add(new Student("a", "a", "a", 1, "1"));
        Assert.assertEquals(5, ReflectionDemo.countObjects(objects));
    }

    @Test
    public void getListMethods() {
        Comparator<String> comparator = String::compareTo;
        Human human = new Human("a", "a", "a", 1);
        ArrayList<String> list = new ArrayList<>(
                List.of("getName", "equals", "hashCode", "compareTo", "compareTo", "setName", "getSurname",
                        "setSurname", "getAge", "setAge", "getMiddlename", "setMiddlename", "wait", "wait",
                        "wait", "toString", "getClass", "notify", "notifyAll"
                       ));
        list.sort(comparator);
        List<String> result = ReflectionDemo.getListMethods(human);
        result.sort(comparator);
        Assert.assertEquals(list, result);
    }

    @Test
    public void getListSuperclasses() {
        Student student = new Student("a", "a", "a", 1, "a");
        Assert.assertEquals(List.of("Human", "java.lang.Object"), ReflectionDemo.getListSuperclasses(student));
    }

    class BablaCar extends Car {
        public BablaCar(String type) {
            super(type);
        }
    }

    @Test
    public void countAndReleaseExecuteTest() {
        List<Object> objectList = new ArrayList<>(
                List.of(new Human("a", "a", "a", 3), new Human("a", "a", "a", 1),
                        new Car("audi"), new BablaCar("aaaaaa")
                       ));
        objectList.add(null);
        Assert.assertEquals(2, ReflectionDemo.countAndReleaseExecute(objectList));
    }

    @Test
    public void getListOfGettersAndSettersTest() {
        List<String> methodsList = new ArrayList<>(List.of("getClass", "getType", "setType"));
        Assert.assertEquals(methodsList, ReflectionDemo.getListOfGettersAndSetters(new Car("Audi")).stream().sorted(Comparator.naturalOrder()).toList());
    }
}