import java.io.Serial;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class House implements Serializable {
    private String cadastalNumber;
    private int adress;
    private Person headOfHouse;
    List<Flat> flatList;

    public House(){}

    public House(String cadastalNumber, int adress, Person headOfHouse, List<Flat> flatList) {
        this.cadastalNumber = cadastalNumber;
        this.adress = adress;
        this.headOfHouse = headOfHouse;
        this.flatList = flatList;
    }


    public String getCadastalNumber() {
        return cadastalNumber;
    }

    public void setCadastalNumber(String cadastalNumber) {
        this.cadastalNumber = cadastalNumber;
    }

    public int getAdress() {
        return adress;
    }

    public void setAdress(int adress) {
        this.adress = adress;
    }

    public Person getHeadOfHouse() {
        return headOfHouse;
    }

    public void setHeadOfHouse(Person headOfHouse) {
        this.headOfHouse = headOfHouse;
    }

    public List<Flat> getFlatList() {
        return flatList;
    }

    public void setFlatList(List<Flat> flatList) {
        this.flatList = flatList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof House house)) return false;
        return adress == house.adress && Objects.equals(cadastalNumber, house.cadastalNumber) && Objects.equals(headOfHouse, house.headOfHouse) && Objects.equals(flatList, house.flatList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cadastalNumber, adress, headOfHouse, flatList);
    }
}
