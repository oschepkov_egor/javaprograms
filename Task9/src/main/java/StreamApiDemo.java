import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class StreamApiDemo extends LambdaDemo {
    public static Function<List<Object>, List<Object>> removeNull = (a) -> a.stream().filter(Objects::nonNull).toList();

    public static Function<Set<Integer>, Long> countPositive = (a) -> a.stream().filter(n -> n >= 0).count();

    public static Function<List<Object>, Object[]> getThreeLast =
            (a) -> (a.size() > 3) ? a.stream().skip(a.size() - 3).toArray() : a.toArray();

    public static Function<List<Integer>, Integer> getFirstEven =
            (a) -> a.stream().filter(n -> n % 2 == 0).findFirst().orElse(null);

    public static Function<Integer[], List<Integer>> getListOfSquares =
            (a) -> Arrays.stream(a).distinct().map(n -> n * n).toList();

    public static Function<List<String>, List<String>> getListOfSortedStrings =
            (a) -> a.stream().filter(n -> (n != null && n.length() > 0)).sorted().toList();

    public static Function<Set<String>, List<String>> getListOfStringReverseSort =
            (a) -> (a.stream().sorted(Comparator.reverseOrder()).toList());

    public static Function<Set<Integer>, Integer> getSumOfSquares = (a) -> a.stream().mapToInt(n -> n * n).sum();
    //    reduce(0,
//                                                                                                             Integer::sum
//                                                                                                            );
    public static Function<Collection<Human>, Integer> getMaxAge =
            (a) -> a.stream().max(Comparator.comparingInt(Human::getAge)).isPresent() ?
                   a.stream().max(Comparator.comparingInt(Human::getAge)).get().getAge() : -1;

    public static Function<Collection<Human>, Collection<Human>> getSortedHumans =
            (a) -> a.stream().sorted(Comparator.comparing(
                    Human::getSex).thenComparing(
                    Human::getAge)).collect(Collectors.toList());
}
