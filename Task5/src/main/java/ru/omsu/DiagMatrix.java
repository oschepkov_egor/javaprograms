package ru.omsu;

public class DiagMatrix extends Matrix {

    public DiagMatrix(int n) {
        super(n);
    }

    public DiagMatrix(int n, double[] diag) {
        super(n);
        for (int i = 0; i < n; i++) {
            setElem(diag[i], i, i);
        }
    }

    @Override
    public void setElem(double v, int i, int j) {
        if ( v != 0 && i != j) {
            throw new IllegalArgumentException("i != j for v != 0");
        }
        super.setElem(v, i, j);
    }
}
