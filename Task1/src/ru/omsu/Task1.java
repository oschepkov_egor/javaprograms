package ru.omsu;

import java.sql.SQLOutput;
import java.util.Scanner;

public class Task1 {
    public static void main(String[] args) {
        testVectorArray();
    }

    public static void print(Vector3D vector, String name){
        System.out.println(name + " (" + (vector.getX()) + " " + (vector.getY()) + " " + (vector.getZ()) + ") = " + vector.length());
    }

    public static void testPoint(){
        Point3D point1 = new Point3D();

        Point3D point2 = new Point3D(1, 2, 3);
        Point3D point3 = new Point3D(1, 2, 3);
        System.out.println(point2.equals(point3));
        //System.out.println("Point2 is equal point3 " + (point2.equals(point3)));
        //System.out.println("Point1 is equal point1 " + (point1.equals(point1)));
    }

    public static void testVector(){
        Vector3D vector1 = new Vector3D(new Point3D(),  new Point3D(1, 1, 1));
        Vector3D vector2 = new Vector3D(1, 1, 1, 2, 4, 4);
        print(vector1, "Vector1");
        print(vector2, "Vector2");
        System.out.println("Vector1 is equal vector2 " + vector1.equals(vector2));
    }

    public static void testVectorProcessor(){
        Vector3D vector1 = new Vector3D(new Point3D(), new Point3D(1, 1, 1));
        Vector3D vector2 = new Vector3D(0,0,0, 2, 2, 2);

        Vector3D vector3 = Vector3DProcessor.summa(vector1, vector2);
        System.out.println(vector1);

        Vector3D vector4 = Vector3DProcessor.difference(vector2, vector1);
        print(vector4, "Vector4");

        System.out.println("Scalar vector1 and vector2 " + Vector3DProcessor.scalar(vector1, vector2));

        Vector3D vector5 = Vector3DProcessor.vector(vector1, vector2);
        print(vector5, "Vector5");
        Vector3D xx = new Vector3D(0,0,0,0.1,0.1,0.1);
        vector1 = Vector3DProcessor.summa(vector1, xx);
        vector1 = Vector3DProcessor.summa(vector1, xx);
        vector1 = Vector3DProcessor.summa(vector1, xx);
        vector1 = Vector3DProcessor.summa(vector1, xx);
        vector1 = Vector3DProcessor.summa(vector1, xx);

        System.out.println("Vector1 collinear vector2 " + Vector3DProcessor.collinear(vector1, vector2));
    }

    public static void testVectorArray(){
        Vector3D vector1 = new Vector3D(new Point3D(), new Point3D(1, 1, 1));
        Vector3D vector2 = new Vector3D(1, 1, 1, 2, 4, 4);
        Vector3D vector3 = new Vector3D(new Point3D(), new Point3D(0, -2, 2));
        Vector3D vector4 = new Vector3D(1, 1, 1, 2, 4, 4);

        Vector3DArray vectors = new Vector3DArray(4);
        vectors.setVector(vector1, 0);
        vectors.setVector(vector2, 1);
        vectors.setVector(vector3, 2);
        vectors.setVector(vector4, 3);
        System.out.println("Max length vectors " + vectors.maxLengthVector());
        System.out.println("Index vector " + vectors.indexOf(vector2));
        Vector3D vector5 = vectors.summaAllVectors();
        print(vector5, "Vector5");
        Vector3D vector6 = vectors.linComVect(new double[]{1, 1, 1, -1});
        print(vector6, "Vector6");
        Point3D[] points = vectors.move(new Point3D(1, 2, 3));
        for (Point3D elem : points) {
            elem.print();
        }
    }

    public static void task1() {
        System.out.println("Hello world");
    }

    public static void task2() {
        Scanner s = new Scanner(System.in);
        double a = 0, b = 0, c = 0;
        a = s.nextDouble();
        b = s.nextDouble();
        c = s.nextDouble();

        System.out.println(a * b * c);
        System.out.println((a + b + c) / 3);
        if (a <= b && a <= c) {
            System.out.print(a + " ");
            if (b <= c) {
                System.out.println(b + " " + c);
            } else {
                System.out.println(c + " " + b);
            }
        } else if (b <= a && b <= c) {
            System.out.print(b + " ");
            if (a <= c) {
                System.out.println(a + " " + c);
            } else {
                System.out.println(c + " " + a);
            }
        } else {
            System.out.print(c + " ");
            if (a <= b) {
                System.out.println(a + " " + b);
            } else {
                System.out.println(b + " " + a);
            }
        }
    }

    public static void task3() {
        Scanner s = new Scanner(System.in);
        int a = 0, b = 0, c = 0;
        a = s.nextInt();
        b = s.nextInt();
        c = s.nextInt();

        System.out.println(a * b * c);
        System.out.println((a + b + c) / 3);
        if (a <= b && a <= c) {
            System.out.print(a + " ");
            if (b <= c) {
                System.out.println(b + " " + c);
            } else {
                System.out.println(c + " " + b);
            }
        } else if (b <= a && b <= c) {
            System.out.print(b + " ");
            if (a <= c) {
                System.out.println(a + " " + c);
            } else {
                System.out.println(c + " " + a);
            }
        } else {
            System.out.print(c + " ");
            if (a <= b) {
                System.out.println(a + " " + b);
            } else {
                System.out.println(b + " " + a);
            }
        }
    }

    public static void task4() {
        Scanner s = new Scanner(System.in);
        double d = 0, x1, x2, a = 0, b = 1, c = 0;
        a = s.nextDouble();
        b = s.nextDouble();
        c = s.nextDouble();


        d = b * b - 4 * a * c;
        if (d >= 0) {
            if (b == 0 && a == 0) {
                System.out.println("Не уравнение");
            } else if (a == 0) {
                System.out.println("x = " + ((-1) * c / b));
            } else {
                x1 = (-b + Math.sqrt(d)) / (2 * a);
                x2 = (-b - Math.sqrt(d)) / (2 * a);
                System.out.println("x1 = " + x1);
                System.out.println("x2 = " + x2);
            }
        } else {
            System.out.println("Нет рациональных корней");
        }
    }

    public static void task5() {
        Scanner s = new Scanner(System.in);
        double a, b, c;
        a = s.nextDouble();
        b = s.nextDouble();
        c = s.nextDouble();

        for (double i = a; i < b + c; i += c) {
            System.out.println("sin (" + i + ") = " + Math.sin(i));
        }
    }

    public static void task6() {
        Scanner s = new Scanner(System.in);
        double a, b, c, d, e, f, k, x, y, deltaX, deltaY, delta = 1;
        a = s.nextDouble();
        b = s.nextDouble();
        c = s.nextDouble();
        d = s.nextDouble();
        e = s.nextDouble();
        f = s.nextDouble();
        System.out.println(a + "x " + b + "y = " + c);
        System.out.println(d + "x " + e + "y = " + f);
        deltaX = c * e - b * f;
        deltaY = a * f - c * d;
        delta = a * e - b * d;
        if (delta == 0) {
            System.out.println("Нет решений");
        } else {
            x = deltaX / delta;
            y = deltaY / delta;
            System.out.println("x = " + x);
            System.out.println("y = " + y);
        }
    }

    public static void task7() {
        double x, n, sum = 1., d = 1.0;


        Scanner s = new Scanner(System.in);

        x = s.nextDouble();
        n = s.nextDouble();

        for (int i = 1; Math.abs(d) > n; i++) {
            d *= x / i;
            sum += d;
            System.out.println("" + i + " " + d + " " + sum);
        }
        /*if (x < 0){
            sum = 1 / sum;
        }*/


        System.out.println(sum);
    }
}