import java.util.Arrays;
import java.util.Objects;

public class FinanceReport {
    private Payment[] payments;
    private String reporterFullName;
    private int reporterDay;
    private int reporterMonth;
    private int reporterYear;

    public FinanceReport(
            Payment[] payments,
            String reporterFullName,
            int reporterDay,
            int reporterMonth,
            int reporterYear
                        ) {
        this.payments = payments;
        this.reporterFullName = reporterFullName;
        this.reporterDay = reporterDay;
        this.reporterMonth = reporterMonth;
        this.reporterYear = reporterYear;
    }

    public FinanceReport(FinanceReport financeReport) {
        //payments = financeReport.payments;
        payments = new Payment[financeReport.payments.length];
        for (int i = 0; i < financeReport.payments.length; i++) {
            payments[i] = new Payment(financeReport.getPayment(i));
        }
        reporterFullName = financeReport.reporterFullName;
        reporterDay = financeReport.reporterDay;
        reporterMonth = financeReport.reporterMonth;
        reporterYear = financeReport.reporterYear;
    }

    public int countPayments() {
        return payments.length;
    }

    public Payment getPayment(int i) {
        return payments[i];
    }

    public void setPayments(Payment p, int i) {
        payments[i] = p;
    }

    public Payment[] getPayments() {
        return payments;
    }

    public void setPayments(Payment[] payments) {
        this.payments = payments;
    }

    public String getReporterFullName() {
        return reporterFullName;
    }

    public void setReporterFullName(String reporterFullName) {
        this.reporterFullName = reporterFullName;
    }

    public int getReporterDay() {
        return reporterDay;
    }

    public void setReporterDay(int reporterDay) {
        this.reporterDay = reporterDay;
    }

    public int getReporterMonth() {
        return reporterMonth;
    }

    public void setReporterMonth(int reporterMonth) {
        this.reporterMonth = reporterMonth;
    }

    public int getReporterYear() {
        return reporterYear;
    }

    public void setReporterYear(int reporterYear) {
        this.reporterYear = reporterYear;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(String.format("[Автор: %s, дата: %d.%d.%d дата создания, Платежи: [\n", reporterFullName, reporterDay,
                                reporterMonth, reporterYear
                               ));
        for (Payment payment : payments) {
            sb.append(String.format("    Плательщик: %s, дата: %d.%d.%d сумма: %d руб. %02d коп.\n,", payment,
                                    payment.getDay(), payment.getMonth(), payment.getYear(),
                                    payment.getSumPayment() / 100, payment.getSumPayment() % 100
                                   ));
        }
        sb.deleteCharAt(sb.length() - 1);
        sb.append("]]");
        return sb.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FinanceReport that = (FinanceReport) o;
        return reporterDay == that.reporterDay && reporterMonth == that.reporterMonth &&
               reporterYear == that.reporterYear && Arrays.equals(
                payments, that.payments) && Objects.equals(reporterFullName, that.reporterFullName);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(reporterFullName, reporterDay, reporterMonth, reporterYear);
        result = 31 * result + Arrays.hashCode(payments);
        return result;
    }
}
