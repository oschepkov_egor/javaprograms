import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReflectionDemo {
    public static int countObjects(List<Object> objects) {
        int i = 0;
        for (Object object : objects) {
            if (object instanceof Human) {
                i++;
            }
        }
        return i;
    }

    public static List<String> getListMethods(Object object) {
        List<String> methods = new ArrayList<>();
        Class<?> c = object.getClass();
        for (Method method : c.getMethods()) {
            methods.add(method.getName());
        }
        return methods;
    }

    public static List<String> getListSuperclasses(Object object) {
        List<String> superclasses = new ArrayList<>();
        Class<?> superclass = object.getClass();
        while (superclass != Object.class) {
            superclass = superclass.getSuperclass();
            superclasses.add(superclass.getName());
        }
        return superclasses;
    }

    public static int countAndReleaseExecute(List<Object> objectList) {
        int n = 0;
        for (Object object : objectList) {
            if (object != null) {
                if (object instanceof IExecutable) {
                    ((IExecutable) object).execute();
                    n++;
                }
            }
        }
        return n;
    }

    public static List<String> getListOfGettersAndSetters(Object object) {
        List<String> resultList = new ArrayList<>();
        for (Method method : object.getClass().getMethods()) {
            if (!Modifier.isStatic(method.getModifiers()) &&
                (method.getParameterCount() == 0 && method.getReturnType() != void.class &&
                 method.getName().startsWith("get") ||
                 method.getName().startsWith("set") && method.getParameterCount() == 1 &&
                 method.getReturnType() == void.class)) {
                resultList.add(method.getName());
            }
        }
        return resultList;
    }
}
