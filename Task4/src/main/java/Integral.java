public class Integral implements IFunctionalWithOneArgument{
    double begin;
    double end;
    int n = 101;

    public Integral(double begin, double end) {
        this.begin = begin;
        this.end = end;
    }

    @Override
    public double getFunctional(IFunctionWithOneArgument func) {
        double res = 0;
        if(func.getBegin() > begin  || func.getEnd() < end){
            throw new IllegalArgumentException("function isn't exist in integral");
        }
        double e = (end - begin) / n;
        for (int i = 0; i < n; i++){
            res += func.getValue(begin + i * e) * e;
        }
        return res;
    }

    public double getBegin() {
        return begin;
    }

    public void setBegin(double begin) {
        this.begin = begin;
    }

    public double getEnd() {
        return end;
    }

    public void setEnd(double end) {
        this.end = end;
    }


}
