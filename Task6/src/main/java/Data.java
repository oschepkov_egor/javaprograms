import java.util.Iterator;

public class Data implements Iterable<Integer> {
    private String name;
    private Group[] groups;

    public Data(String name, Group... groups) {
        this.name = name;
        this.groups = groups;
    }

    public int length() {
        return groups.length;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Group[] getGroups() {
        return groups;
    }

    public void setGroups(Group[] groups) {
        this.groups = groups;
    }

    @Override
    public Iterator<Integer> iterator() {
        return new Iterator<Integer>() {

            private int idData = 0;
            private int idGroup = -1;

            @Override
            public boolean hasNext() {
                if (groups == null || groups.length == 0) {
                    return false;
                }
                if (idGroup != -1 && idData < groups.length - 1 && groups[idData + 1].length() == 0) {
                    for (int i = idData + 1; i < groups.length; i++) {
                        if (groups[idData + i].length() != 0) {
                            return true;
                        }
                    }
                    return false;
                }
                return !(idData == groups.length - 1 && idGroup >= groups[idData].getData().length - 1);
            }

            @Override
            public Integer next() {
                if (idGroup + 1 == groups[idData].getData().length && idData + 1 != groups.length) {
                    idGroup = 0;
                    idData += 1;
                    while (idData < groups.length && groups[idData].getData().length == 0) {
                        idData++;
                    }
                } else {
                    idGroup++;
                }
                return groups[idData].getData()[idGroup];
            }
        };
    }
}
