public interface IFunctionWithOneArgument {
    double getValue(double x);
    double getBegin();
    double getEnd();
}
