import java.util.Objects;

public class PackagedWeightGood extends WeightGood implements IGood{
    private PackagedOfGood packagedOfGood;
    private double weightOfGoods;

    public double getNetto(){
        return weightOfGoods;
    }

    public double getBrutto(){
        return weightOfGoods + packagedOfGood.getWeight();
    }

    public PackagedWeightGood(Good good, PackagedOfGood packagedOfGood, double weightOfGoods) {
        super(good.getName(), good.getDescription());
        this.packagedOfGood = packagedOfGood;
        this.weightOfGoods = weightOfGoods;
    }

    public PackagedOfGood getPackagedOfGood() {
        return packagedOfGood;
    }

    public void setPackagedOfGood(PackagedOfGood packagedOfGood) {
        this.packagedOfGood = packagedOfGood;
    }

    public void setWeightOfGoods(int weightOfGoods) {
        this.weightOfGoods = weightOfGoods;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PackagedWeightGood that)) return false;
        if (!super.equals(o)) return false;
        return weightOfGoods == that.weightOfGoods && Objects.equals(packagedOfGood, that.packagedOfGood);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), packagedOfGood, weightOfGoods);
    }

}
