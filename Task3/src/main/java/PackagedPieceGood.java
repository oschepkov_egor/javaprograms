import java.util.Objects;

public class PackagedPieceGood extends PieceGood implements IGood{

    private PackagedOfGood packagedOfGood;
    private int countOfGood;

    public PackagedPieceGood(String name,
                             String description,
                             double weightByOne,
                             PackagedOfGood packagedOfGood,
                             int countOfGood) {
        super(name, description, weightByOne);
        this.packagedOfGood = packagedOfGood;
        this.countOfGood = countOfGood;
    }

    public double getNetto(){
        return countOfGood * this.getWeightByOne();
    }

    public double getBrutto(){
        return getNetto() + packagedOfGood.getWeight();
    }

    public PackagedOfGood getPackagedOfGood() {
        return packagedOfGood;
    }

    public void setPackagedOfGood(PackagedOfGood packagedOfGood) {
        this.packagedOfGood = packagedOfGood;
    }

    public int getCountOfGood() {
        return countOfGood;
    }

    public void setCountOfGood(int countOfGood) {
        this.countOfGood = countOfGood;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PackagedPieceGood that)) return false;
        if (!super.equals(o)) return false;
        return countOfGood == that.countOfGood && Objects.equals(packagedOfGood, that.packagedOfGood);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), packagedOfGood, countOfGood);
    }
}
