package ru.omsu;

import java.util.Objects;

public class Point3D {

    private double x;
    private double y;
    private double z;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point3D point3D = (Point3D) o;
        double eps = 1e-4;
        return Math.abs(point3D.x - x) < eps && Math.abs(point3D.y - y) < eps && Math.abs(point3D.z - z) < eps;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y, z);
    }

    public void print(){
        System.out.println("x: " + x + " y: " + y + " z: " + z);
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getZ() {
        return z;
    }

    public Point3D(double x, double y, double z) {
        setX(x);
        setY(y);
        setZ(z);
    }

    public Point3D(){
    }
}
