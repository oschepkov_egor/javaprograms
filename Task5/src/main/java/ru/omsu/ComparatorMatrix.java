package ru.omsu;

import java.util.Comparator;

public class ComparatorMatrix implements Comparator<IMatrix> {
    @Override
    public int compare(IMatrix o1, IMatrix o2) {
        return (int) (o1.getDet() - o2.getDet());
    }
}
