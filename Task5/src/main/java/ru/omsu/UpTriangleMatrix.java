package ru.omsu;

public class UpTriangleMatrix extends Matrix {
    public UpTriangleMatrix(int n) {
        super(n);
    }

    @Override
    public void setElem(double v, int i, int j) {
        if (v != 0 && i > j) {
            throw new IllegalArgumentException("invalid index for v != 0");
        }
        super.setElem(v, i, j);
    }
}
