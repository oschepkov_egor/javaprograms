import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class FinanceReportProcessorTest {
    @Test
    public void testCountPayments() {
        Payment[] payments = {new Payment("Егор", 1, 1, 1, 100), new Payment("Саша",
                                                                             2, 4, 2, 80
        ),
                              new Payment("Сеня", 3, 2, 1, 75)};
        FinanceReport financeReport = new FinanceReport(payments, "Никита", 3, 4, 1);
        FinanceReport financeReportByChar = FinanceReportProcessor.getFinanceReportByChar(financeReport, 'С');
        Assert.assertEquals(
                new FinanceReport(new Payment[]{new Payment("Саша", 2, 4, 2, 80), new Payment("Сеня", 3, 2, 1, 75)},
                                  "Никита", 3, 4, 1
                ), financeReportByChar);
    }

    @Test
    public void testWithoutCountPayments() {
        Payment[] payments = {new Payment("Егор", 1, 1, 1, 100), new Payment("Саша", 2, 4, 2, 80), new Payment("Сеня",
                                                                                                               3, 2, 1,
                                                                                                               75
        )};
        FinanceReport financeReport = new FinanceReport(payments, "Никита", 3, 4, 1);
        FinanceReport financeReportByChar = FinanceReportProcessor.getFinanceReportByChar(financeReport, 'Д');
        Assert.assertEquals(new FinanceReport(new Payment[]{}, "Никита", 3, 4, 1), financeReportByChar);
    }

    @Test
    public void testSummaPayments() {
        Payment[] payments = {new Payment("Егор", 1, 1, 1, 100), new Payment("Саша", 2, 4, 2, 80), new Payment("Сеня",
                                                                                                               3, 2, 1,
                                                                                                               200
        )};
        FinanceReport financeReport = new FinanceReport(payments, "Никита", 3, 4, 1);
        FinanceReport financeReportByChar = FinanceReportProcessor.getFinanceReportBySumma(financeReport, 150);
        Assert.assertEquals(
                new FinanceReport(new Payment[]{new Payment("Егор", 1, 1, 1, 100), new Payment("Саша", 2, 4, 2, 80)},
                                  "Никита", 3, 4, 1
                ), financeReportByChar);
    }

    @Test
    public void testWithoutSummaPayments() {
        Payment[] payments = {new Payment("Егор", 1, 1, 1, 100), new Payment("Саша", 2, 4, 2, 80), new Payment("Дима",
                                                                                                               3, 2, 1,
                                                                                                               200
        )};
        FinanceReport financeReport = new FinanceReport(payments, "Никита", 3, 4, 1);
        FinanceReport financeReportBySumma = FinanceReportProcessor.getFinanceReportBySumma(financeReport, 0);
        Assert.assertEquals(new FinanceReport(new Payment[]{}, "Никита", 3, 4, 1), financeReportBySumma);
    }

    @Test
    public void testSummaPaymentsByDate() {
        Payment[] payments = {new Payment("Егор", 1, 1, 1, 100), new Payment("Саша", 2, 4, 2, 80), new Payment("Дима",
                                                                                                               1, 1, 1,
                                                                                                               200
        )};
        FinanceReport financeReport = new FinanceReport(payments, "Никита", 3, 4, 1);
        Assert.assertEquals(300, FinanceReportProcessor.getFullSummaPaymentByDate(financeReport, "1.1.1"), 1e-4);
    }

    @Test
    public void testListOfMonthsByYear() {
        Payment[] payments = {new Payment("Егор", 1, 2, 1, 100), new Payment("Саша", 2, 4, 2, 80), new Payment("Дима",
                                                                                                               1, 1, 1,
                                                                                                               200
        )};
        FinanceReport financeReport = new FinanceReport(payments, "Никита", 3, 4, 1);
        Assert.assertEquals("Март Апрель Май Июнь Июль Август Сентябрь Октябрь Ноябрь Декабрь",
                            FinanceReportProcessor.getMonthWithoutPaymentsByYear(financeReport, 1)
                           );
    }
}
