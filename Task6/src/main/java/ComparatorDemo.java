import java.util.ArrayList;
import java.util.Collections;
import java.util.stream.Collectors;

public class ComparatorDemo {
    public static void sortHumans(ArrayList<Human> humans, HumanComparator humanComparator) {
        humans.sort(humanComparator);
        Collections.reverse(humans);
    }
}
