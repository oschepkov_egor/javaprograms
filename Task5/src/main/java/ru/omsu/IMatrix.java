package ru.omsu;

public interface IMatrix {
    int getSize();
    double getElem(int i, int j);
    void setElem(double v, int i, int j);
    double getDet();
}
