public interface IFunctionalWithOneArgument<T extends IFunctionWithOneArgument> {
    double getFunctional(T func);
}
