import org.junit.Assert;
import org.junit.Test;


public class SquareTrinomialWithBiggerRootsTest {
    @Test(expected = IllegalArgumentException.class)
    public void testGetBiggerRootWithException() {
        SquareTrinomialWithBiggerRoots squareTrinomialWithBiggerRoots = new SquareTrinomialWithBiggerRoots(new SquareTrinomial(1, 2, 3));
        Assert.assertEquals(1, squareTrinomialWithBiggerRoots.getBiggerRoot(), 1e-4);
    }

    @Test
    public void testGetRoot() {
        SquareTrinomialWithBiggerRoots squareTrinomialWithBiggerRoots = new SquareTrinomialWithBiggerRoots(new SquareTrinomial(1, 2, -3));
        Assert.assertEquals(1, squareTrinomialWithBiggerRoots.getBiggerRoot(), 1e-4);
    }
}