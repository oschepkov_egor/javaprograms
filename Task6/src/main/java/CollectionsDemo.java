import java.util.*;

public class CollectionsDemo {
    public static int countStringsWithFirstChar(List<String> list, char c) {
        int count = 0;
        for (String string : list) {
            if (string != null && !"".equals(string) && string.charAt(0) == c) {
                count++;
            }
        }
        return count;
    }

    public static List<Human> getListWithEqualsFirstname(List<Human> humans, Human human) {
        List<Human> resultHumans = new ArrayList<>();
        for (Human iHuman : humans) {
            if (iHuman.getSurname().equals(human.getSurname())) {
                resultHumans.add(iHuman);
            }
        }
        return resultHumans;
    }

    public static List<Human> copyListWithoutHuman(List<Human> humans, Human human) {
        List<Human> resultHumans = new ArrayList<>();
        for (Human iHuman : humans) {
            if (iHuman.equals(human)) {
                continue;
            }
            resultHumans.add(new Human(iHuman));
        }
        return resultHumans;
    }

    public static List<Set<Integer>> getListSetsNotIntersectionWithSet(List<Set<Integer>> sets,
                                                                       Set<Integer> set) {
        List<Set<Integer>> resultSets = new ArrayList<>();
        for (Set<Integer> iSet : sets) {
            var temporarySet = new HashSet<>(iSet);
            temporarySet.retainAll(set);
            if (temporarySet.isEmpty()) {
                resultSets.add(iSet);
            }
        }
        return resultSets;
    }

    public static Set<? extends Human> getListHumansWithMaxAge(List<? extends Human> humans) {
        Set<Human> resultHumans = new HashSet<>();
        int max = -1;
        for (Human human : humans) {
            if (human.getAge() >= max) {
                if (human.getAge() > max) {
                    max = human.getAge();
                    resultHumans.clear();
                }
                resultHumans.add(human);
            }
        }
        return resultHumans;
    }

    public static List<Human> getSortListByHuman(Set<Human> set){
        TreeSet<Human> treeSet = new TreeSet<>(set);
        return new ArrayList<>(treeSet);
    }

    public static Set<Human> getSetHumansWithId(Map<Integer, Human> map, Set<Integer> set) {
        Set<Human> resultSet = new HashSet<>();
        for (int key : set) {
            if (map.containsKey(key)) {
                resultSet.add(map.get(key));
            }
        }
        return resultSet;
    }

    public static List<Integer> getListIdWithAgeLower18(Map<Integer, Human> map) {
        List<Integer> resultList = new ArrayList<>();

        for (var item : map.entrySet()) {
            if (item.getValue().getAge() >= 18) {
                resultList.add(item.getKey());
            }
        }
        return resultList;
    }

    public static Map<Integer, Integer> getMapWithIdAge(Map<Integer, Human> map) {
        Map<Integer, Integer> resultMap = new HashMap<>();

        for (int key : map.keySet()) {
            resultMap.put(key, map.get(key).getAge());
        }

        return resultMap;
    }

    public static Map<Integer, List<Human>> getMapWithIdAgeValueListOfHumans(Set<Human> set) {
        Map<Integer, List<Human>> resultMap = new HashMap<>();

        for (Human human : set) {
            if (!resultMap.containsKey(human.getAge())) {
                ArrayList<Human> humans = new ArrayList<>();
                humans.add(human);
                resultMap.put(human.getAge(), humans);
            } else {
                resultMap.get(human.getAge()).add(human);
            }
        }
        for (var item: resultMap.entrySet()){
            Collections.sort(item.getValue());
        }
        return resultMap;
    }

    public static Map<Integer, Map<Character, List<Human>>> getMapByAgeMappingByCharInHuman(Set<Human> set) {
        Map<Integer, List<Human>> mapByAge = getMapWithIdAgeValueListOfHumans(set);
        Map<Integer, Map<Character, List<Human>>> resultMap = new HashMap<>();
        for (int keyByAge : mapByAge.keySet()) {
            resultMap.put(keyByAge, new HashMap<>());
            for (Human human : mapByAge.get(keyByAge)) {
                if (!resultMap.get(keyByAge).containsKey(human.getSurname().charAt(0))) {
                    resultMap.get(keyByAge).put(human.getSurname().charAt(0), new ArrayList<>());
                }
                resultMap.get(keyByAge).get(human.getSurname().charAt(0)).add(human);
            }
        }

        for (int keyByAge : resultMap.keySet()) {
            for (char keyByChar : resultMap.get(keyByAge).keySet()) {
                ComparatorDemo.sortHumans((ArrayList<Human>) resultMap.get(keyByAge).get(keyByChar), new HumanComparator());
            }
        }

        return resultMap;
    }
}
