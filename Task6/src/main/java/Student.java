public class Student extends Human {
    private String nameFaculty;

    public Student(String firstname, String name, String lastname, int age, String nameFaculty) {
        super(firstname, name, lastname, age);
        this.nameFaculty = nameFaculty;
    }

    public Student(Human human, String nameFaculty) {
        super(human);
        this.nameFaculty = nameFaculty;
    }

    public String getNameFaculty() {
        return nameFaculty;
    }

    public void setNameFaculty(String nameFaculty) {
        this.nameFaculty = nameFaculty;
    }
}
