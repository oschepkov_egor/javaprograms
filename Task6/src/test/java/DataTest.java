import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class DataTest {
    @Test
    public void getDataAllTest1() {
        Data data = new Data("A", new Group(1, 1, 2, 3, 5), new Group(2, 2, 3, 6), new Group(3, 1), new Group(4, 3, 2), new Group(5, 7, 4, 4, 2, 1));
        List<Integer> list = List.of(1, 2, 3, 5, 2, 3, 6, 1, 3, 2, 7, 4, 4, 2, 1);
        Assert.assertEquals(list, DataDemo.getAll(data));
    }

    @Test
    public void getDataAllTest2() {
        Data data = new Data("A");
        List<Integer> list = List.of();
        Assert.assertEquals(list, DataDemo.getAll(data));
    }

    @Test
    public void getDataAllTest3() {
        Data data = new Data("A", new Group(1));
        List<Integer> list = List.of();
        Assert.assertEquals(list, DataDemo.getAll(data));
    }

    @Test
    public void getDataAllTest4() {
        Data data = new Data("A", new Group(1, 1), new Group(2),new Group(3), new Group(4, 1));
        List<Integer> list = List.of(1, 1);
        Assert.assertEquals(list, DataDemo.getAll(data));
    }

    @Test
    public void getDataAllTest5() {
        Data data = new Data("A", null);
        List<Integer> list = List.of();
        Assert.assertEquals(list, DataDemo.getAll(data));
    }

    @Test
    public void getDataAllTest6() {
        Data data = new Data("A", new Group(1, 1), new Group(2));
        List<Integer> list = List.of(1);
        Assert.assertEquals(list, DataDemo.getAll(data));
    }
}
