import java.util.Objects;

public class ExpFunction implements IFunctionWithOneArgument {
    private double a;
    private double b;
    private double begin;
    private double end;

    @Override
    public double getValue(double x) {
        if(x < begin || x > end) throw new IllegalArgumentException("x isn't [begin, end]");
        return a*Math.exp(x) + b;
    }

    @Override
    public double getBegin() {
        return begin;
    }

    @Override
    public double getEnd() {
        return end;
    }

    public ExpFunction(double a, double b, double begin, double end) {
        this.a = a;
        this.b = b;
        this.begin = begin;
        this.end = end;
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public void setBegin(double begin) {
        this.begin = begin;
    }

    public void setEnd(double end) {
        this.end = end;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ExpFunction that)) return false;
        return Double.compare(that.a, a) == 0 && Double.compare(that.b, b) == 0 && Double.compare(that.begin, begin) == 0 && Double.compare(that.end, end) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(a, b, begin, end);
    }
}
