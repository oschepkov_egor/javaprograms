import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class ComparatorDemoTest {

    @Test
    public void sortGoods() {
        Good[] goods = new Good[]{new WeightGood("B", "A"), new WeightGood("A", "B"), new WeightGood("A", "A")};

        ComparatorDemo.sortGoods(goods, new GoodComparator());

        Assert.assertArrayEquals(new Good[]{new WeightGood("A", "A"), new WeightGood("A", "B"), new WeightGood("B", "A")}, goods);
    }
}