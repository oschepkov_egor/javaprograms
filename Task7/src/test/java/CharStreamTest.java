import junit.framework.TestCase;
import org.junit.Assert;
import java.nio.charset.StandardCharsets;

import java.io.*;

public class CharStreamTest extends TestCase {

    public void testWriteReadStream() throws IOException {
        int[] a = {1, 2, 300000000};
        try (Writer writer = new FileWriter("a.txt", StandardCharsets.UTF_8)) {
            CharStream.writeStream(writer, a);
        }
        int[] b = new int[a.length];
        try (Reader reader = new FileReader("a.txt", StandardCharsets.UTF_8)) {
            CharStream.readStream(reader, b);
        }
        Assert.assertArrayEquals(a, b);
    }
}