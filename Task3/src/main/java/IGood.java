public interface IGood{
    double getBrutto();
    double getNetto();
    String getName();
}
