package ru.omsu;

public class Vector3DArray {
    private double eps = 1e-4;
    private Vector3D[] array;

    public Vector3DArray(int n) {
        array = new Vector3D[n];
        for (int i = 0; i < n; i++) {
            array[i] = new Vector3D();
        }
    }

    public int length() {
        return array.length;
    }

    public void setVector(Vector3D vector, int i) {
        array[i] = vector;
    }

    public double maxLengthVector() {
        double max = -1;
        for (Vector3D elem : array) {
            if (max < elem.length()) {
                max = elem.length();
            }
        }
        return max;
    }

    public int indexOf(Vector3D vector) {
        int n = -1;
        for (int i = 0; i < array.length; i++) {
            if (array[i].equals(vector)) {
                n = i;
                break;
            }
        }
        return n;
    }

    public Vector3D summaAllVectors() {
        Vector3D vector = new Vector3D();
        for (Vector3D elem : array) {
            vector = Vector3DProcessor.summa(vector, elem);
        }
        return vector;
    }

    public Vector3D linComVect(double[] k) {
        if (k.length != array.length) {
            return new Vector3D();
        }
        Vector3D vector = new Vector3D();
        for (int i = 0; i < array.length; i++) {
            vector.setX(vector.getX() + k[i]*(array[i].getX()));
            vector.setY(vector.getY() + k[i]*(array[i].getY()));
            vector.setZ(vector.getZ() + k[i]*(array[i].getZ()));
        }
        return vector;
    }

    public Point3D[] move(Point3D point) {
        Point3D[] points = new Point3D[array.length];
        for (int i = 0; i < array.length; i++) {
            points[i] = new Point3D(point.getX() + array[i].getX(), point.getY() + array[i].getY(), point.getZ() + array[i].getZ());
        }
        return points;
    }
}
