import org.junit.Assert;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.assertEquals;

public class CollectionsDemoTest {
    @Test
    public void countStringsWithFirstCharTest() {
        ArrayList<String> list = new ArrayList<String>();
        list.add("abc");
        list.add("bdc");
        list.add("adb");
        Assert.assertEquals(2, CollectionsDemo.countStringsWithFirstChar(list, 'a'));
    }

    @Test
    public void getListWithEqualsFirstnameTest() {
        ArrayList<Human> humans = new ArrayList<Human>();
        humans.add(new Human("Lee", "Bruce", "Dan", 15));
        humans.add(new Human("Nill", "Bruce", "Paul", 20));
        humans.add(new Human("Lee", "Marvin", "Jon", 21));

        ArrayList<Human> resultHumans = new ArrayList<Human>();
        resultHumans.add(new Human("Lee", "Bruce", "Dan", 15));
        resultHumans.add(new Human("Lee", "Marvin", "Jon", 21));

        Human human = new Human("Lee", "Jon", "Bruce", 12);

        Assert.assertEquals(resultHumans, CollectionsDemo.getListWithEqualsFirstname(humans, human));
    }

    @Test
    public void copyListWithoutHumanTest() {
        ArrayList<Human> humans = new ArrayList<Human>();
        humans.add(new Human("Lee", "Bruce", "Dan", 15));
        humans.add(new Human("Nill", "Bruce", "Paul", 20));
        humans.add(new Human("Lee", "Marvin", "Jon", 21));

        ArrayList<Human> resultHumans = new ArrayList<Human>();
        resultHumans.add(new Human("Lee", "Bruce", "Dan", 15));
        resultHumans.add(new Human("Lee", "Marvin", "Jon", 21));

        Human human = new Human("Nill", "Bruce", "Paul", 20);
        assertEquals(resultHumans, CollectionsDemo.copyListWithoutHuman(humans, human));
    }

    @Test
    public void copyListWithoutHumanItemsStaticTest() {
        ArrayList<Human> humans = new ArrayList<>();
        humans.add(new Human("Lee", "Bruce", "Dan", 15));

        Human human = new Human("Nill", "Bruce", "Paul", 20);

        ArrayList<Human> resultHumans = (ArrayList<Human>) CollectionsDemo.copyListWithoutHuman(humans, human);

        humans.get(0).setAge(16);

        Assert.assertNotEquals(resultHumans.get(0), humans.get(0));
    }

    @Test
    public void getListHumansWithMaxAgeTest() {
        ArrayList<Human> humans = new ArrayList<>();

        Student student1 = new Student("A", "B", "C", 21, "imit");
        humans.add(student1);

        Human human = new Human("B", "C", "d", 21);
        humans.add(human);

        Student student2 = new Student("d", "s", "e", 14, "fcs");
        humans.add(student2);

        HashSet<Human> resultHumans = new HashSet<>();
        resultHumans.add(student1);
        resultHumans.add(human);

        Assert.assertEquals(resultHumans, CollectionsDemo.getListHumansWithMaxAge(humans));
    }

    /*@Test
    public void getSortListByHuman(){
        Set<Human> humans = new HashSet<>();
        humans.add(new Human("F", "B", "C",11));
        humans.add(new Student("G", "A", "E",15, "A"));
        humans.add(new Human("B", "B", "D",12));

        List<Human> result = new ArrayList<>();
        result.add(new Human("B", "B", "D",12));
        result.add(new Human("F", "B", "C",11));
        result.add(new Student("G", "A", "E",15, "A"));

        Assert.assertEquals(result, CollectionsDemo.getSortListByHuman(humans));
    }*/

    @Test
    public void getListSetsNotIntersectionWithSetTest() {
        ArrayList<Set<Integer>> sets = new ArrayList<>();

        HashSet<Integer> set1 = new HashSet<>();
        set1.add(1);
        set1.add(2);
        set1.add(3);
        sets.add(set1);

        HashSet<Integer> set2 = new HashSet<>();
        set2.add(2);
        set2.add(3);
        set2.add(4);
        sets.add(set2);

        HashSet<Integer> set3 = new HashSet<>();
        set3.add(5);
        set3.add(6);
        set3.add(7);
        sets.add(set3);

        HashSet<Integer> set = new HashSet<>();
        set.add(3);
        set.add(4);

        ArrayList<HashSet<Integer>> resultSets = new ArrayList<>();
        resultSets.add(new HashSet<>(set3));

        Assert.assertEquals(resultSets, CollectionsDemo.getListSetsNotIntersectionWithSet(sets, set));
    }

    @Test
    public void getSortListByHumanTest() {
        Set<Human> humans = new HashSet<>(
                Set.of(new Human("a", "a", "b", 10), new Human("a", "a", "b", 6), new Human("a", "a", "a", 10)));
        List<Human> listHumans = CollectionsDemo.getSortListByHuman(humans);
        Assert.assertEquals(
                List.of(new Human("a", "a", "a", 10), new Human("a", "a", "b", 6), new Human("a", "a", "b", 10)),
                listHumans
                           );
    }

    @Test
    public void getSetHumansWithIdTest() {
        HashMap<Integer, Human> map = new HashMap<>();
        map.put(1, new Human("A", "B", "C", 12));
        map.put(2, new Human("B", "C", "A", 20));
        map.put(3, new Human("C", "A", "B", 25));

        HashSet<Integer> set = new HashSet<>();
        set.add(1);
        set.add(3);
        set.add(4);

        HashSet<Human> resultSet = new HashSet<>();
        resultSet.add(new Human("A", "B", "C", 12));
        resultSet.add(new Human("C", "A", "B", 25));

        Assert.assertEquals(resultSet, CollectionsDemo.getSetHumansWithId(map, set));
    }

    @Test
    public void getListIdWithAgeLower18Test() {
        HashMap<Integer, Human> map = new HashMap<>();
        map.put(1, new Human("A", "B", "C", 12));
        map.put(2, new Human("B", "C", "A", 20));
        map.put(3, new Human("C", "A", "B", 18));

        ArrayList<Integer> resultList = new ArrayList<>();
        resultList.add(2);
        resultList.add(3);

        Assert.assertEquals(resultList, CollectionsDemo.getListIdWithAgeLower18(map));
    }

    @Test
    public void getMapWithIdAgeTest() {
        HashMap<Integer, Human> map = new HashMap<>();
        map.put(1, new Human("A", "B", "C", 12));
        map.put(2, new Human("B", "C", "A", 20));
        map.put(3, new Human("C", "A", "B", 17));

        HashMap<Integer, Integer> resultMap = new HashMap<>();
        resultMap.put(1, 12);
        resultMap.put(2, 20);
        resultMap.put(3, 17);

        Assert.assertEquals(resultMap, CollectionsDemo.getMapWithIdAge(map));
    }

    @Test
    public void getMapWithIdAgeValueListOfHumansTest() {
        Set<Human> set = new HashSet<>();
        set.add(new Human("A", "B", "C", 12));
        set.add(new Human("B", "C", "A", 20));
        set.add(new Human("C", "A", "B", 12));

        Map<Integer, List<Human>> resultMap = new HashMap<>();

        List<Human> humans1 = new ArrayList<>();
        humans1.add(new Human("A", "B", "C", 12));
        humans1.add(new Human("C", "A", "B", 12));
        Collections.sort(humans1);
        resultMap.put(12, humans1);

        List<Human> humans2 = new ArrayList<>();
        humans2.add(new Human("B", "C", "A", 20));
        resultMap.put(20, humans2);
        Assert.assertEquals(resultMap, CollectionsDemo.getMapWithIdAgeValueListOfHumans(set));
    }

    @Test
    public void getMapByAgeMappingByCharInHuman() {
        HashSet<Human> set = new HashSet<>();
        set.add(new Human("A", "B", "C", 12));
        set.add(new Human("B", "C", "A", 20));
        set.add(new Human("Ca", "A", "B", 12));
        set.add(new Human("C", "B", "B", 14));
        set.add(new Human("Cb", "B", "A", 12));
        set.add(new Human("Cb", "A", "A", 12));
        set.add(new Human("D", "A", "A", 14));
        set.add(new Human("C", "A", "B", 20));

        Map<Integer, Map<Character, List<Human>>> map = CollectionsDemo.getMapByAgeMappingByCharInHuman(set);

        Map<Integer, Map<Character, List<Human>>> resultMap = new HashMap<>();

        List<Human> humans1 = new ArrayList<>();
        humans1.add(new Human("Cb", "B", "A", 12));
        humans1.add(new Human("Cb", "A", "A", 12));
        humans1.add(new Human("Ca", "A", "B", 12));
        List<Human> humans2 = new ArrayList<>();
        humans2.add(new Human("A", "B", "C", 12));
        Map<Character, List<Human>> map1 = new HashMap<>();
        map1.put('C', humans1);
        map1.put('A', humans2);
        resultMap.put(12, map1);

        humans1 = new ArrayList<>();
        humans1.add(new Human("D", "A", "A", 14));
        humans2 = new ArrayList<>();
        humans2.add(new Human("C", "B", "B", 14));
        map1 = new HashMap<>();
        map1.put('D', humans1);
        map1.put('C', humans2);
        resultMap.put(14, map1);

        humans1 = new ArrayList<>();
        humans1.add(new Human("C", "A", "B", 20));
        humans2 = new ArrayList<>();
        humans2.add(new Human("B", "C", "A", 20));
        map1 = new HashMap<>();
        map1.put('C', humans1);
        map1.put('B', humans2);
        resultMap.put(20, map1);

        Assert.assertEquals(resultMap, map);
    }


}