public class SquareTrinomial {
    private double a;
    private double b;
    private double c;

    public double[] getRoots(){
        double d = b*b - 4*a*c;
        double[] roots;
        if(d > 0){
            roots = new double[2];
            roots[0] = (-b + Math.sqrt(d)) / 2 / a;
            roots[1] = (-b - Math.sqrt(d)) / 2 / a;
            return roots;
        }else if(d == 0){
            roots = new double[1];
            roots[0] = -b / 2 / a;
            return roots;
        }
        roots = new double[0];
        return roots;
    }

    public SquareTrinomial(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public double getA() {
        return a;
    }

    public void setA(double a) {
        this.a = a;
    }

    public double getB() {
        return b;
    }

    public void setB(double b) {
        this.b = b;
    }

    public double getC() {
        return c;
    }

    public void setC(double c) {
        this.c = c;
    }
}
