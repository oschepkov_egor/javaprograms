import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.HashMap;

public class PhoneBookTest {

    @Test
    public void getListByNameTest() {
        PhoneBook phoneBook = new PhoneBook();
        phoneBook.addPhone("A", "11");
        phoneBook.addPhone("A", "12");
        phoneBook.addPhone("B", "13");

        ArrayList<String> list = new ArrayList<>();
        list.add("11");
        list.add("12");

        Assert.assertEquals(list, phoneBook.getListByName("A"));
    }

    @Test
    public void getNameByPhoneTest() {
        PhoneBook phoneBook = new PhoneBook();
        phoneBook.addPhone("A", "11");
        phoneBook.addPhone("A", "12");
        phoneBook.addPhone("B", "13");

        Assert.assertEquals("B", phoneBook.getNameByPhone("13"));
    }

    @Test
    public void getMapByBeginNameTest() {
        PhoneBook phoneBook = new PhoneBook();
        phoneBook.addPhone("Aa", "11");
        phoneBook.addPhone("Ab", "12");
        phoneBook.addPhone("B", "13");

        PhoneBook resultPhoneBook = new PhoneBook();
        resultPhoneBook.addPhone("Aa", "11");
        resultPhoneBook.addPhone("Ab", "12");

        Assert.assertEquals(resultPhoneBook, phoneBook.getMapByBeginName("A"));
    }
}
