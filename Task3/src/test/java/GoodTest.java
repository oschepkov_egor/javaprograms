import org.junit.Assert;
import org.junit.Test;

public class GoodTest {
    @Test
    public void bruttoTest() {
        BatchOfGoods batchOfGoods1 = new BatchOfGoods("pack", new IGood[]{new PackagedWeightGood(new Good("A", "Aa"), new PackagedOfGood("Aaa", 3), 2), new PackagedPieceGood("B", "Bb", 1, new PackagedOfGood("Bbb", 2), 2)});
        Assert.assertEquals(9, batchOfGoods1.getAllBrutto(), 1e-4);
        BatchOfGoods batchOfGoods2 = new BatchOfGoods("pack", new IGood[]{new PackagedWeightGood(new Good("A", "Aa"), new PackagedOfGood("Aaa", 4), 3), new PackagedPieceGood("B", "Bb", 2, new PackagedOfGood("Bbb", 2), 3)});
        Assert.assertEquals(15, batchOfGoods2.getAllBrutto(), 1e-4);
    }

    @Test
    public void serviceOfGoodsTest() {
        Assert.assertEquals(1, ServiceOfGoods.countByFilter(new BatchOfGoods("A", new IGood[]{new PackagedWeightGood(new Good("A", "Aa"), new PackagedOfGood("Aaa", 3), 2), new PackagedPieceGood("B", "Bb", 1, new PackagedOfGood("Bbb", 2), 2)}), new BeginStringFilter("A")));
        Assert.assertEquals(2, ServiceOfGoods.countByFilter(new BatchOfGoods("B", new IGood[]{new PackagedWeightGood(new Good("Aa", "Aa"), new PackagedOfGood("Aaa", 3), 2), new PackagedPieceGood("A", "Bb", 1, new PackagedOfGood("Bbb", 2), 2)}), new BeginStringFilter("A")));
    }

    @Test
    public void packageSetOfGoodsTest() {
        PackagedSetOfGoods packagedSetOfGoods = new PackagedSetOfGoods(new PackagedOfGood("A", 1), new IGood[]{new PackagedPieceGood("A", "A", 10, new PackagedOfGood("A", 1), 1), new PackagedSetOfGoods(new PackagedOfGood("A", 1), new IGood[]{new PackagedPieceGood("A", "A", 10, new PackagedOfGood("A", 1), 1), new PackagedSetOfGoods(new PackagedOfGood("A", 1), new IGood[]{new PackagedPieceGood("A", "A", 10, new PackagedOfGood("A", 1), 1)})})});
        Assert.assertEquals(36, packagedSetOfGoods.getBrutto(), 1e-4);
    }

    @Test
    public void countByFilterDeepTest() {
        PackagedSetOfGoods packagedSetOfGoods = new PackagedSetOfGoods(new PackagedOfGood("A", 1), new IGood[]{new PackagedPieceGood("A", "A", 10,new PackagedOfGood("A", 1), 1), new PackagedSetOfGoods(new PackagedOfGood("A", 1), new IGood[]{new PackagedPieceGood("A", "A", 10, new PackagedOfGood("A", 1), 1), new PackagedSetOfGoods(new PackagedOfGood("A", 1), new IGood[]{new PackagedPieceGood("B", "A", 10, new PackagedOfGood("A", 1), 1)})}), new PackagedWeightGood(new WeightGood("B", ""), new PackagedOfGood("B", 1), 5)});
        BatchOfGoods batchOfGoods = new BatchOfGoods("", packagedSetOfGoods.getGoods());
        Assert.assertEquals(ServiceOfGoods.countByFilterDeep(batchOfGoods, new BeginStringFilter("B")), 2);
    }

    @Test
    public void checkAllWeightedTest() {
        BatchOfGoods batchOfGoods = new BatchOfGoods("", new IGood[]{new PackagedWeightGood(new WeightGood("", ""), new PackagedOfGood("", 1), 5), new PackagedWeightGood(new WeightGood("", ""), new PackagedOfGood("", 1), 5)});
        Assert.assertTrue(ServiceOfGoods.checkAllWeighted(batchOfGoods));
    }

    @Test
    public void checkNotAllWeightedTest() {
        BatchOfGoods batchOfGoods = new BatchOfGoods("", new IGood[]{new PackagedPieceGood("", "" , 1, new PackagedOfGood("", 1), 1), new PackagedWeightGood(new WeightGood("", ""), new PackagedOfGood("", 1), 5)});
        Assert.assertFalse(ServiceOfGoods.checkAllWeighted(batchOfGoods));
    }
}
